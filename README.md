# `jvc-payment` Repository

[![pipeline status](https://gitlab.com/johnjvester/jvc-payment/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/jvc-payment/commits/master)


> The `jvc-payment` repository is a [Spring Boot](https://spring.io/projects/spring-boot) RESTful 
> API which is designed to utilize a [Heroku Postgres](https://www.heroku.com/postgres) database 
> and [CloudAMQP](https://elements.heroku.com/addons/cloudamqp) messaging service, 
> running within the [Heroku](https://www.heroku.com/home) ecosystem. The service is intended to 
> house domain information related to the `Payment` objects.

## Service Design

This repository is part of the following distributed microservice prototype:

![Heroku Ecosystem](./HerokuDesign.png)

## Related Repositories

This repository is related to the following public repositories:

* [`jvc-order`](https://gitlab.com/johnjvester/jvc-order)
* [`jvc-customer`](https://gitlab.com/johnjvester/jvc-customer)
* [`jvc-payment`](https://gitlab.com/johnjvester/jvc-payment) - _this repository_

## Publications

This repository is related to an article published on DZone.com:

* TBD

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Important Information

The following environment settings need to be configured:

* `${DATABASE_URL}` (`spring.datasource.url`) - URL to Heroku Postgres database
* `${CLOUDAMQP_URL}` (`spring.rabbitmq.addresses`) - URL to CloudAMQP service
* `${PORT}` (`server.port`) - port number for Spring Boot service

The SQL for the associated Heroku Postgres database can be found in the following location:

`./payments-postgres.sql`

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.