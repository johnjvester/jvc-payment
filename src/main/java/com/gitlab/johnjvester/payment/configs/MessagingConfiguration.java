package com.gitlab.johnjvester.payment.configs;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
@EnableRabbit
public class MessagingConfiguration {
    private final MessagingConfigurationProperties messagingConfigurationProperties;

    @Bean(name = "paymentDirectExchange")
    public DirectExchange paymentDirectExchange() {
        return new DirectExchange(messagingConfigurationProperties.getPaymentDirectExchange());
    }

    @Bean(name = "paymentRequestQueue")
    public Queue paymentRequestQueue() {
        return new Queue(messagingConfigurationProperties.getPaymentRequestQueue());
    }

    @Bean
    public Binding binding(DirectExchange paymentDirectExchange,
                           Queue queue) {
        return BindingBuilder.bind(queue)
                .to(paymentDirectExchange)
                .with(messagingConfigurationProperties.getPaymentRoutingKey());
    }

    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
