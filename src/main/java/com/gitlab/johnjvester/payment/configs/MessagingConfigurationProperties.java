package com.gitlab.johnjvester.payment.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Data
@Configuration("messagingConfigurationProperties")
@ConfigurationProperties(prefix = "messaging")
public class MessagingConfigurationProperties {
    @NotNull
    private String paymentDirectExchange;

    @NotNull
    private String paymentRequestQueue;

    @NotNull
    private String paymentRoutingKey;
}
