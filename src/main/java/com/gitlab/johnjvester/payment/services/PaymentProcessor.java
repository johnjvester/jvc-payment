package com.gitlab.johnjvester.payment.services;

import com.gitlab.johnjvester.payment.entities.Payment;
import com.gitlab.johnjvester.payment.models.PaymentDto;
import com.gitlab.johnjvester.payment.repositories.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class PaymentProcessor {
    private final PaymentRepository paymentRepository;

    @RabbitListener(queues = "#{messagingConfigurationProperties.paymentRequestQueue}")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PaymentDto receive(PaymentDto paymentDto) {
        log.debug("PaymentProcessor: receive(paymentDto={})", paymentDto);

        Payment payment = new Payment();
        payment.setAmount(paymentDto.getAmount());
        payment.setCustomerId(paymentDto.getCustomerId());
        payment.setTransactionId(UUID.randomUUID().toString());
        paymentRepository.save(payment);

        paymentDto.setId(payment.getId());
        paymentDto.setTransactionId(payment.getTransactionId());

        log.info("payment={}", payment);
        return paymentDto;
    }
}
