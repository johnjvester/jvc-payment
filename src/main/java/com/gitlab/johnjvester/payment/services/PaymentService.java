package com.gitlab.johnjvester.payment.services;

import com.gitlab.johnjvester.payment.entities.Payment;
import com.gitlab.johnjvester.payment.repositories.PaymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;

    public List<Payment> getPayments() {
        return paymentRepository.findAll();
    }

    public Payment getPayment(int id) {
        return paymentRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }
}
