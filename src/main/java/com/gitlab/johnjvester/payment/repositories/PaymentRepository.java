package com.gitlab.johnjvester.payment.repositories;

import com.gitlab.johnjvester.payment.entities.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Integer> { }
