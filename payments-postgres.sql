CREATE TABLE payments (
                          id INT PRIMARY KEY NOT NULL,
                          transaction_id VARCHAR(36) NOT NULL,
                          amount DECIMAL(12,2),
                          customer_id INT NOT NULL
);